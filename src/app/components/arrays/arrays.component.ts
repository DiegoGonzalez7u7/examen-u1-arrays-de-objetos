import { Component } from '@angular/core';

@Component({
  selector: 'app-arrays',
  templateUrl: './arrays.component.html',
  styleUrls: ['./arrays.component.css']
})
export class ArraysComponent {
// estudiar la estructura de un arreglo
// arreglo: []
// objeto {}
// array de objetos [{}]
   arreglo=[ //array
    { //objeto
      objeto1:{
        primer:"objeto 1.1", //atributo
        segundo:"objeto 1.2",
        tercero:"objeto 1.3",
      },
      objeto2:{
        primer:"objeto 2.1",
        segundo:"objeto 2.2",
        tercer:"objeto 2.3"
      },
      objeto3:{
        primer:"objeto 3.1",
        segundo:"objeto 3.2",
        tercer:"objeto 3.3",
      },
      objeto4:{
        primer:"objeto 4.1",
        segundo:"objeto 4.2",
        tercero:"objeto 4.3",
      },
      objeto5:{
        primer:"objeto 5.1",
        segundo:"objeto 5.2",
        tercer:"objeto 5.3"
      },
      objeto6:{
        primer:"objeto 5.1",
        segundo:"objeto 5.2",
        tercer:"objeto 5.3",
      }
    }
  ]
  examen=true; //variable verdadera para estructura ngif
}
